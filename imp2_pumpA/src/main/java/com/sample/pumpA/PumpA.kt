package com.sample.pumpA

import com.sample.pump.IPump

class PumpA : IPump {

    override fun getName(): String = "pumpA"

    override fun getFamily(): String? = null
}