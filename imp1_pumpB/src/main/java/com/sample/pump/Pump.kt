package com.sample.pump

class Pump : IPump {

    override fun getName(): String = "pumpB"

    override fun getFamily(): String = "FamilyB"
}