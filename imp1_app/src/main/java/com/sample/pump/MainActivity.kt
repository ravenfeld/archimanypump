package com.sample.pump

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sample.pump.databinding.MainActivityBinding

class MainActivity : AppCompatActivity() {


    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val pump = Pump()
        binding.name.text = pump.getName()
        pump.getFamily()?.let {
            binding.family.text = it
            binding.family.visibility = View.VISIBLE
        }
    }
}