package com.sample.pumpB

import com.sample.pump.IPump

class PumpB : IPump {

    override fun getName(): String = "pumpB"

    override fun getFamily(): String = "FamilyB"
}