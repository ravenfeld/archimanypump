package com.sample.pump

interface IPump {

    fun getName(): String

    fun getFamily(): String?
}